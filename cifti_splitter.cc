#include <algorithm>
#include <exception>
#include <string>

#include <armadillo>

#include "CiftiLib/CiftiFile.h"
#include "NewNifti/NewNifti.h"
#include "giftiio/giftiio.h"


#include "cifti_splitter.h"


using namespace std;
using namespace cifti;
using namespace giftiio;


namespace cifti_tools {

  /**
   * Estimates scaling parameters from an affine transformation matrix.
   *
   * Voxel scales / pixdims are not stored in CIFTI files, so we have to glean
   * them from the sform (which is available).
   *
   * Spencer W. Thomas, Decomposing a matrix into simple transformations, pp
   * 320-323 in *Graphics Gems II*, James Arvo (editor), Academic Press, 1991,
   * ISBN: 0120644819.
   */
  static vector<float> get_scales(const NiftiIO::mat44 & xform) {
    vector<double> vals(12);
    for (int i = 0; i < 12; i++) {
      vals[i] = xform.m[i % 3][i / 3];
    }
    arma::mat33   x = arma::reshape(arma::mat(vals), 3, 3);
    arma::rowvec M1 = x.row(0);
    arma::rowvec M2 = x.row(1);
    arma::rowvec M3 = x.row(2);

    float sx = std::sqrt(arma::sum(M1 % M1));
    M1 = M1 / sx;
    float sxy = arma::sum(M1 % M2);
    M2 = M2 - sxy * M1;

    float sy = std::sqrt(arma::sum(M2 % M2));

    M2 = M2 / sy;

    float sxz = arma::sum(M1 % M3);
    float syz = arma::sum(M2 % M3);

    M3 = M3 - sxz * M1 - syz * M2;

    float sz = std::sqrt(arma::sum(M3 % M3));

    vector<float> scales(3);
    scales[0] = sx;
    scales[1] = sy;
    scales[2] = sz;

    return scales;
  }


  /**
   * Create a CiftiSplitter for a CIFTI file.
   *
   * Identifies the model / data dimensions and lengths, and loads information
   * about the data stored in the file via read_volume_information and
   * read_surface_information.
   *
   * Throws a std::invalid_argument error if the CIFTI file is not of a
   * compatible type.
   */
  CiftiSplitter::CiftiSplitter(string filename) :
    cfile(filename),
    hdr(NiftiIO::loadHeader(filename)) {

    auto xml = cfile.getCiftiXML();

    if (xml.getNumberOfDimensions() != 2) {
      throw invalid_argument(filename + " contains more than 2 dimensions");
    }

    model_dim = -1;
    data_dim  = -1;

    for (int dim = 0; dim < xml.getNumberOfDimensions(); dim++) {
      auto mtype = xml.getMappingType(dim);

      if (mtype == CiftiMappingType::MappingType::BRAIN_MODELS ||
          mtype == CiftiMappingType::MappingType::PARCELS) {
        model_dim = dim;
      }
      else if (mtype == CiftiMappingType::MappingType::SERIES  ||
               mtype == CiftiMappingType::MappingType::SCALARS ||
               mtype == CiftiMappingType::MappingType::LABELS) {
        data_dim = dim;
      }
      // parcel: could output .time.gii / .shape.gii / .func.gii,
      // but could also output .label.gii denoting the parcels
      else {
        throw invalid_argument("Unrecognised dimension type: " + mtype);
      }
    }

    if (model_dim == -1 || data_dim == -1) {
      throw invalid_argument(filename + ": Couldn't identify "
                             "brain model and data dimensions");
    }

    data_type  = hdr.datatype;
    num_models = xml.getDimensionLength(model_dim);
    num_data   = xml.getDimensionLength(data_dim);

    auto mtype = xml.getMappingType(model_dim);

    if (mtype == CiftiMappingType::BRAIN_MODELS) {
      auto map        = xml.getBrainModelsMap(model_dim);
      has_volume_data = read_volume_information(map);
    }
    else { // PARCELS
      auto map        = xml.getParcelsMap(model_dim);
      has_volume_data = read_volume_information(map);
    }
    read_surface_information();
  }


  /**
   * Returns a vector containing identifiers for all CIFTI structures
   * for which the CIFTI file contains data.
   */
  const std::vector<cifti::StructureEnum::Enum> &
  CiftiSplitter::surface_structures() {
    return surf_strucs;
  }


  /**
   * Returns the NIFTI data type contained in the CIFTI file (e.g.
   * NiftiIO::DT_FLOAT).
   */
  short CiftiSplitter::dtype() {
    return data_type;
  }

  /**
   * Called at construction. Populates the CiftiSplitter fields containing
   * information about the volumetric data, including dimensions, pixdims,
   * and the sform. Does nothing if the CIFTI file does not contain any
   * volumetric data.
   */
  template<class T>
  bool CiftiSplitter::read_volume_information(const T & map) {

    auto xml = cfile.getCiftiXML();

    if (!map.hasVolumeData()) {
      return false;
    }

    auto spc     = map.getVolumeSpace();
    auto csform  = spc.getSform();
    int64_t xlen = spc.getDims()[0];
    int64_t ylen = spc.getDims()[1];
    int64_t zlen = spc.getDims()[2];

    // convert ciftilib sform
    // repr to niftiio repr
    for (int i = 0; i < 16; i++) {
      sform.m[i % 4][i / 4] = csform[i % 4][i / 4];
    }

    // dims / pixdims
    vector<float> scales = get_scales(sform);
    volume_dims  .assign({xlen,      ylen,      zlen});
    volume_scales.assign({scales[0], scales[1], scales[2]});

    // ciftilib returns the sform scaled such
    // that its world space is defined in mm. We
    // have to interrogate the XML to figure out
    // the series units and step-length (which
    // will be used to set pixdim4).
    space_units = NiftiIO::NIFTI_UNITS_MM;
    if (xml.getMappingType(data_dim) == CiftiMappingType::MappingType::SERIES) {
      auto smap  = xml.getSeriesMap(data_dim);
      auto sunit = smap.getUnit();
      time_step  = smap.getStep();
      if (sunit == CiftiSeriesMap::Unit::SECOND) {
        time_units |= NiftiIO::NIFTI_UNITS_SEC;
      }
      else if (sunit == CiftiSeriesMap::Unit::HERTZ) {
        time_units |= NiftiIO::NIFTI_UNITS_HZ;
      }
      else if (sunit == CiftiSeriesMap::Unit::RADIAN) {
        time_units |= NiftiIO::NIFTI_UNITS_RADS;
      }
    }
    return true;
  }


  /**
   * Called at construction. Populates the CiftiSplitter fields containing
   * information about the surface data, including the structures that are
   * present. Does nothing if the CIFTI file does not contain any
   * surface data.
   */
  void CiftiSplitter::read_surface_information() {

    auto xml   = cfile.getCiftiXML();
    auto mtype = xml.getMappingType(model_dim);

    if (mtype == CiftiMappingType::BRAIN_MODELS) {
      auto map    = xml.getBrainModelsMap(model_dim);
      auto strucs = map.getSurfaceStructureList();
      surf_strucs.assign(strucs.begin(), strucs.end());
    }
    else { // PARCELS
      auto map    = xml.getParcelsMap(model_dim);
      auto strucs = map.getParcelSurfaceStructures();
      surf_strucs.assign(strucs.begin(), strucs.end());
    }
  }


  /**
   * Read a row/column of data from the brain model/parcel dimension of
   * the CIFTI file.
   */
  void CiftiSplitter::read_row(std::vector<float> &rowdata, int64_t row) {
    if (model_dim == cifti::CiftiXML::ALONG_ROW) {
      cfile.getRow(rowdata.data(), row);
    }
    else {
      cfile.getColumn(rowdata.data(), row);
    }
  }


  /**
   * Initialise a NEWIMAGE::volume for storing volumetric data from the CIFTI
   * file. This boils down to setting the dimensions, pixdims, and sform.
   */
  template<class T> void CiftiSplitter::initialise_volume(
    NEWIMAGE::volume<T> & vol, int ndata) {

    // convert sform from niftiio repr
    // to newimage/newmage/arma repr
    arma::Mat<double> asform(4, 4);
    for (int i = 0; i < 16; i++) {
      asform(i / 4, i % 4) = sform.m[i / 4][i % 4];
    }

    vol.reinitialize(volume_dims[0],
                     volume_dims[1],
                     volume_dims[2],
                     ndata);
    vol.setdims(     volume_scales[0],
                     volume_scales[1],
                     volume_scales[2],
                     time_step);
    vol.set_sform(2, asform);
    vol.set_qform(2, asform);
  }


  /* Template instantiations for different NEWIMAGE::volume<T> types. */
  template void CiftiSplitter::initialise_volume<float>( NEWIMAGE::volume<float>  & vol, int ndata);
  template void CiftiSplitter::initialise_volume<double>(NEWIMAGE::volume<double> & vol, int ndata);
  template void CiftiSplitter::initialise_volume<char>(  NEWIMAGE::volume<char>   & vol, int ndata);
  template void CiftiSplitter::initialise_volume<short>( NEWIMAGE::volume<short>  & vol, int ndata);
  template void CiftiSplitter::initialise_volume<int>(   NEWIMAGE::volume<int>    & vol, int ndata);


  /**
   * Extract the volumetric data stored in the CIFTI file as a NEWIMAGE volume.
   *
   * Returns false if the CIFTI file does not contain volumetric data, true
   * otherwise.
   */
  template<class T>
  bool CiftiSplitter::volume_data(NEWIMAGE::volume<T> & vol) {

    if (!has_volume_data) {
      return false;
    }

    initialise_volume(vol, num_data);

    auto xml   = cfile.getCiftiXML();
    auto mtype = xml.getMappingType(model_dim);

    if (mtype == CiftiMappingType::BRAIN_MODELS) {
      volume_data_from_brain_models(vol);
    }
    else { // PARCELS
      volume_data_from_parcels(vol);
    }

    return true;
  }

  template<class T>
  void CiftiSplitter::volume_data_from_brain_models(NEWIMAGE::volume <T> & vol) {

    auto xml   = cfile.getCiftiXML();
    auto map   = xml.getBrainModelsMap(model_dim);
    auto slope = hdr.sclSlope;
    auto inter = hdr.sclInter;

    vector<float> rowdata(num_models);
    for (int row = 0; row < num_data; row++) {

      read_row(rowdata, row);

      for (auto struc : map.getVolumeStructureList()) {
        auto voxels = map.getVolumeStructureMap(struc);
        for (auto v : voxels) {
          int x             = v.m_ijk[0];
          int y             = v.m_ijk[1];
          int z             = v.m_ijk[2];
          vol(x, y, z, row) = rowdata[v.m_ciftiIndex] * slope + inter;
        }
      }
    }
  }

  template<class T>
  void CiftiSplitter::volume_data_from_parcels(NEWIMAGE::volume <T> & vol) {
    auto xml   = cfile.getCiftiXML();
    auto map   = xml.getParcelsMap(model_dim);
    auto slope = hdr.sclSlope;
    auto inter = hdr.sclInter;

    vector<float> rowdata(num_models);
    for (int row = 0; row < num_data; row++) {

      read_row(rowdata, row);

      for (auto parcel : map.getParcels()) {
        for (auto v : parcel.m_voxelIndices) {
          int64_t x         = v.m_ijk[0];
          int64_t y         = v.m_ijk[1];
          int64_t z         = v.m_ijk[2];
          int64_t idx       = map.getIndexForVoxel(x, y, z);
          vol(x, y, z, row) = rowdata[idx] * slope + inter;
        }
      }
    }
  }

  /* Template instantiations for different NEWIMAGE::volume<T> types. */
  template bool CiftiSplitter::volume_data<float>(                   NEWIMAGE::volume<float>  & vol);
  template bool CiftiSplitter::volume_data<double>(                  NEWIMAGE::volume<double> & vol);
  template bool CiftiSplitter::volume_data<char>(                    NEWIMAGE::volume<char>   & vol);
  template bool CiftiSplitter::volume_data<short>(                   NEWIMAGE::volume<short>  & vol);
  template bool CiftiSplitter::volume_data<int>(                     NEWIMAGE::volume<int>    & vol);
  template void CiftiSplitter::volume_data_from_brain_models<float>( NEWIMAGE::volume<float>  & vol);
  template void CiftiSplitter::volume_data_from_brain_models<double>(NEWIMAGE::volume<double> & vol);
  template void CiftiSplitter::volume_data_from_brain_models<char>(  NEWIMAGE::volume<char>   & vol);
  template void CiftiSplitter::volume_data_from_brain_models<short>( NEWIMAGE::volume<short>  & vol);
  template void CiftiSplitter::volume_data_from_brain_models<int>(   NEWIMAGE::volume<int>    & vol);
  template void CiftiSplitter::volume_data_from_parcels<float>(      NEWIMAGE::volume<float>  & vol);
  template void CiftiSplitter::volume_data_from_parcels<double>(     NEWIMAGE::volume<double> & vol);
  template void CiftiSplitter::volume_data_from_parcels<char>(       NEWIMAGE::volume<char>   & vol);
  template void CiftiSplitter::volume_data_from_parcels<short>(      NEWIMAGE::volume<short>  & vol);
  template void CiftiSplitter::volume_data_from_parcels<int>(        NEWIMAGE::volume<int>    & vol);


  /**
   * Populate a NEWIMAGE::volume with the CIFTI structure labels associated
   * with each volumetric voxel stored in the CIFTI file.
   *
   * Returns false if the CIFTI file does not contain volumetric data, true
   * otherwise.
   */
  template<class T>
  bool CiftiSplitter::volume_labels(NEWIMAGE::volume<T> & vol) {

    auto xml   = cfile.getCiftiXML();
    auto mtype = xml.getMappingType(model_dim);

    if (!has_volume_data) {
      return false;
    }
    if (mtype != CiftiMappingType::BRAIN_MODELS) {
      return false;
    }

    initialise_volume(vol, 1);

    volume_labels_from_brain_models(vol);

    return true;
  }


  template<class T>
  void CiftiSplitter::volume_labels_from_brain_models(
    NEWIMAGE::volume<T> & vol) {
    auto xml = cfile.getCiftiXML();
    auto map = xml.getBrainModelsMap(model_dim);

    for (auto struc : map.getVolumeStructureList()) {
      auto voxels = map.getVolumeStructureMap(struc);
      for (auto v : voxels) {
        int x           = v.m_ijk[0];
        int y           = v.m_ijk[1];
        int z           = v.m_ijk[2];
        vol(x, y, z, 0) = struc;
      }
    }
  }


  /* Template instantiations for different NEWIMAGE::volume<T> types. */
  template bool CiftiSplitter::volume_labels<float>(                   NEWIMAGE::volume<float>  & vol);
  template bool CiftiSplitter::volume_labels<double>(                  NEWIMAGE::volume<double> & vol);
  template bool CiftiSplitter::volume_labels<char>(                    NEWIMAGE::volume<char>   & vol);
  template bool CiftiSplitter::volume_labels<short>(                   NEWIMAGE::volume<short>  & vol);
  template bool CiftiSplitter::volume_labels<int>(                     NEWIMAGE::volume<int>    & vol);
  template void CiftiSplitter::volume_labels_from_brain_models<float>( NEWIMAGE::volume<float>  & vol);
  template void CiftiSplitter::volume_labels_from_brain_models<double>(NEWIMAGE::volume<double> & vol);
  template void CiftiSplitter::volume_labels_from_brain_models<char>(  NEWIMAGE::volume<char>   & vol);
  template void CiftiSplitter::volume_labels_from_brain_models<short>( NEWIMAGE::volume<short>  & vol);
  template void CiftiSplitter::volume_labels_from_brain_models<int>(   NEWIMAGE::volume<int>    & vol);


  /**
   * Extract surface data for the specified structure, returning the
   * data in an armadillo matrix of shape (nvertices, ndatapoints).
   *
   * The matrix will have a row for every vertex in the associated surface,
   * which may be greater than the number of values stored in the CIFTI.
   *
   * Returns false if the CIFTI file does not contain surface data, true
   * otherwise.
   */
  template<class T>
  bool CiftiSplitter::surface_data(StructureEnum::Enum struc,
                                   arma::Mat<T>      & data) {

    if (surf_strucs.size() == 0) {
      return false;
    }
    if (std::count(surf_strucs.begin(), surf_strucs.end(), struc) == 0) {
      return false;
    }

    auto xml   = cfile.getCiftiXML();
    auto mtype = xml.getMappingType(model_dim);

    if (mtype == CiftiMappingType::BRAIN_MODELS) {
      surface_data_from_brain_models(struc, data);
    }
    else { // PARCELS
      surface_data_from_parcels(struc, data);
    }

    return true;
  }

  /* Extract surface data from CIFTI brain models */
  template<class T>
  void CiftiSplitter::surface_data_from_brain_models(
    cifti::StructureEnum::Enum struc,
    arma::Mat<T>             & data) {

    auto xml       = cfile.getCiftiXML();
    auto map       = xml.getBrainModelsMap(model_dim);
    auto num_verts = map.getSurfaceNumberOfNodes(struc);
    auto verts     = map.getSurfaceMap(struc);
    auto slope     = hdr.sclSlope;
    auto inter     = hdr.sclInter;

    data.resize(num_verts, num_data);

    vector<float> rowdata(num_models);

    for (int row = 0; row < num_data; row++) {

      read_row(rowdata, row);

      for (auto vert : verts) {
        float val = rowdata[vert.m_ciftiIndex] * slope + inter;
        data(vert.m_surfaceNode, row) = val;
      }
    }
  }

  /* Extract surface data from CIFTI parcels */
  template<class T>
  void CiftiSplitter::surface_data_from_parcels(
    cifti::StructureEnum::Enum struc,
    arma::Mat<T>             & data) {

    auto xml       = cfile.getCiftiXML();
    auto map       = xml.getParcelsMap(model_dim);
    auto num_verts = map.getSurfaceNumberOfNodes(struc);
    auto slope     = hdr.sclSlope;
    auto inter     = hdr.sclInter;

    data.resize(num_verts, num_data);

    vector<float> rowdata(num_models);

    for (int row = 0; row < num_data; row++) {

      read_row(rowdata, row);

      for (auto parcel : map.getParcels()) {
        auto verts = parcel.m_surfaceNodes[struc];
        for (auto vert : verts) {
          int64_t idx = map.getIndexForNode(vert, struc);
          float   val = rowdata[idx] * slope + inter;
          data(vert, row) = val;
        }
      }
    }
  }


  /* Template instantiations for different arma::Mat<T> types. */
  template bool CiftiSplitter::surface_data<float>(                   StructureEnum::Enum struc, arma::Mat<float>  & data);
  template bool CiftiSplitter::surface_data<double>(                  StructureEnum::Enum struc, arma::Mat<double> & data);
  template bool CiftiSplitter::surface_data<short>(                   StructureEnum::Enum struc, arma::Mat<short>  & data);
  template bool CiftiSplitter::surface_data<int>(                     StructureEnum::Enum struc, arma::Mat<int>    & data);
  template bool CiftiSplitter::surface_data<long>(                    StructureEnum::Enum struc, arma::Mat<long>   & data);
  template void CiftiSplitter::surface_data_from_brain_models<float>( StructureEnum::Enum struc, arma::Mat<float>  & data);
  template void CiftiSplitter::surface_data_from_brain_models<double>(StructureEnum::Enum struc, arma::Mat<double> & data);
  template void CiftiSplitter::surface_data_from_brain_models<short>( StructureEnum::Enum struc, arma::Mat<short>  & data);
  template void CiftiSplitter::surface_data_from_brain_models<int>(   StructureEnum::Enum struc, arma::Mat<int>    & data);
  template void CiftiSplitter::surface_data_from_brain_models<long>(  StructureEnum::Enum struc, arma::Mat<long>   & data);
  template void CiftiSplitter::surface_data_from_parcels<float>(      StructureEnum::Enum struc, arma::Mat<float>  & data);
  template void CiftiSplitter::surface_data_from_parcels<double>(     StructureEnum::Enum struc, arma::Mat<double> & data);
  template void CiftiSplitter::surface_data_from_parcels<short>(      StructureEnum::Enum struc, arma::Mat<short>  & data);
  template void CiftiSplitter::surface_data_from_parcels<int>(        StructureEnum::Enum struc, arma::Mat<int>    & data);
  template void CiftiSplitter::surface_data_from_parcels<long>(       StructureEnum::Enum struc, arma::Mat<long>   & data);

  /**
   * Extract surface data for the specified structure, returning the
   * data as a giftiio::gifti_image. The gifti_image must be freed via
   * giftiio::gifti_free_image when it is no longer needed.
   *
   * Returns false if the CIFTI file does not contain surface data, true
   * otherwise.
   */
  gifti_image * CiftiSplitter::surface_data(
    StructureEnum::Enum struc,
    string            & suffix) {

    arma::fmat data;
    if (!surface_data(struc, data)) {
      return NULL;
    }

    auto xml   = cfile.getCiftiXML();
    auto mtype = xml.getMappingType(data_dim);

    int intent;

    if (mtype == cifti::CiftiMappingType::MappingType::SERIES) {
      intent = NiftiIO::NIFTI_INTENT_TIME_SERIES;
      suffix.assign(".time.gii");
    }
    else if (mtype == cifti::CiftiMappingType::MappingType::SCALARS) {
      intent = NiftiIO::NIFTI_INTENT_SHAPE;
      suffix.assign(".shape.gii");

    }
    else if (mtype == cifti::CiftiMappingType::MappingType::LABELS) {
      intent = NiftiIO::NIFTI_INTENT_LABEL;
      suffix.assign(".label.gii");
    }

    int nrows  = (int)data.n_rows;
    int ncols  = (int)data.n_cols;
    int dims[] = {nrows, ncols};
    int ndims;
    if (data.n_cols == 1) { ndims = 1; }
    else                  { ndims = 2; }

    gifti_image *img = gifti_create_image(
      1, intent, NiftiIO::DT_FLOAT, ndims, dims, 1);

    if (img == NULL) {
      throw std::bad_alloc();
    }

    for (int row = 0; row < nrows; row++) {
    for (int col = 0; col < ncols; col++) {
      int idx = row * data.n_cols + col;
      ((float *)(img->darray[0]->data))[idx] = data(row, col);
    }}

    return img;
  }
}
