#ifndef __CIFTI_SPLITTER_H__
#define __CIFTI_SPLITTER_H__

#include <cstdint>
#include <string>

#include <armadillo>

#include "NewNifti/NewNifti.h"
#include "CiftiLib/CiftiFile.h"
#include "newimage/newimage.h"
#include "giftiio/giftiio.h"


namespace cifti_tools {

  /**
   * The CiftiSplitter can be used to split/separate a CIFTI file into its
   * constituent components.
   *
   * Volumetric data can be extracted as a NEWIMAGE::volume object, and
   * surface data can be extracted as a giftio::gifti_image object.
   *
   * The CiftiSplitter will only work with 2D CIFTI files, where one dimension
   * corresponds to brain models or parcels  (voxels / vertices), and the other
   * dimension corresponds to series, scalars, or labels.
   */
  class CiftiSplitter {

  private:

    /********************
     * General properties
     *******************/

    /* Reference to the CIFTI file */
    cifti::CiftiFile cfile;

    /* NIFTI header */
    NiftiIO::NiftiHeader hdr;

    /* Index of voxel/vertex dimension in the CIFTI */
    int model_dim;

    /* Index of data dimension in the CIFTI */
    int data_dim;

    /* Data type (e.g. NiftiIO::DT_FLOAT) */
    short data_type;

    /* Number of voxels/vertices */
    int64_t num_models;

    /* Number of data points */
    int64_t num_data;

    /*****************************************************
     * Surface properties, set by read_surface_information
     ****************************************************/

    /* Structures for which the CIFTI file contains surface data */
    std::vector<cifti::StructureEnum::Enum> surf_strucs;

    /***************************************************
     * Volume properties, set by read_volume_information
     **************************************************/

    /* Whether or not the CIFTI has volume/voxel data */
    bool has_volume_data;

    /* Number of voxels along each spatial (x, y, z) dimension */
    std::vector<int64_t> volume_dims;

    /* Voxel sizes / pixdims */
    std::vector<float> volume_scales;

    /* Spatial units (e.g. NIFTI_UNITS_MM) */
    int space_units;

    /* Data units (e.g. NIFTI_UNITS_SEC) */
    int time_units;

    /* Data step length (e.g. TR / pixdim4) */
    int time_step;

    /* Voxel -> world/mm affine */
    NiftiIO::mat44 sform;

    /* Called at construction. Reads volume information from the CIFTI */
    template<class T>
    bool read_volume_information(const T & map);

    /* Called at construction. Reads surface information from the CIFTI */
    void read_surface_information();

    /* Read one row of data from the CIFTI  file. */
    void read_row(std::vector<float> &rowdata, int64_t row);

    /* Initialise a NEWIMAGE::volume for storing volume data from the CIFTI */
    template<class T>
    void initialise_volume(NEWIMAGE::volume<T> & vol, int ndata);

    /* Extract volume data from CIFTI brain models */
    template<class T>
    void volume_data_from_brain_models(NEWIMAGE::volume <T> & vol);

    /* Extract volume data from CIFTI parcels */
    template<class T>
    void volume_data_from_parcels(NEWIMAGE::volume <T> & vol);

    /* Extract volume labels from CIFTI brain models */
    template<class T>
    void volume_labels_from_brain_models(NEWIMAGE::volume <T> & vol);

    /* Extract surface  data from CIFTI brain models */
    template<class T>
    void surface_data_from_brain_models(cifti::StructureEnum::Enum struc,
                                        arma::Mat<T>             & data);

    /* Extract surface data from CIFTI parcels */
    template<class T>
    void surface_data_from_parcels(cifti::StructureEnum::Enum struc,
                                   arma::Mat<T>             & data);


  public:

    /* Create a CiftiSplitter for a CIFTI file */
    CiftiSplitter(std::string filename);

    /* Return a vector of identifiers for all surface structures
       that the CIFTI file contains data for. */
    const std::vector<cifti::StructureEnum::Enum> & surface_structures();

    /* Return the NIFTI data type code */
    short dtype();

    /* Extract volume data as a NEWIMAGE volume */
    template<class T>
    bool volume_data(NEWIMAGE::volume<T> & vol);

    /* Populate a NEWIMAGE volume with CIFTI structure labels */
    template<class T>
    bool volume_labels(NEWIMAGE::volume<T> & vol);

    /* Extract surface data for the specified structure, returning the
       data in an armadillo matrix. */
    template<class T>
    bool surface_data(
      cifti::StructureEnum::Enum struc,
      arma::Mat<T>             & data);

    /* Extract surface data for the specified structure, returning the
       data as a giftiio::gifti_image. The gifti_image must be freed via
       giftiio::gifti_free_image when it is no longer needed. */
    giftiio::gifti_image * surface_data(
      cifti::StructureEnum::Enum struc,
      std::string              & suffix);
  };
}

#endif /* __CIFTI_SPLITTER_H__ */
