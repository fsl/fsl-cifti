include ${FSLCONFDIR}/default.mk

PROJNAME     = fsl-cifti
XFILES       = split_cifti
SOFILES      = libfsl-cifti.so

OBJS         = cifti_splitter.o
LIBS         = -lfsl-newimage -lfsl-NewNifti -lfsl-giftiio -lfsl-znz
USRCXXFLAGS  = $(shell pkg-config CiftiLib --cflags)
USRCXXFLAGS += -DEXPOSE_TREACHEROUS
USRLDFLAGS   = $(shell pkg-config CiftiLib --libs)

all: ${XFILES} ${SOFILES}

clean:
	rm -f ${XFILES} ${SOFILES} *.o depend.mk

%.o: %.cc
	$(CXX) $(CXXFLAGS) -c -o $@ $<

libfsl-cifti.so: ${OBJS}
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}

split_cifti: split_cifti.o ${OBJS}
	$(CXX) $(CXXFLAGS) -o $@ $^ ${LDFLAGS}
