#include <iostream>
#include <string>
#include <vector>

#include "newimage/newimageall.h"
#include "giftiio/giftiio.h"

#include "cifti_splitter.h"


using namespace std;


int main(int argc, char *argv[]) {

  if (argc != 3) {
    cout << "Usage: split_cifti <in_file> <out_prefix>" << endl;
    exit(1);
  }

  string infile(NEWIMAGE::return_validimagefilename(argv[1]));
  string outprefix(argv[2]);

  cifti_tools::CiftiSplitter split(argv[1]);

  NEWIMAGE::volume<float> vol;
  NEWIMAGE::volume<int>   labels;

  if (split.volume_data(vol)) {
    cout << "Saving volume data to " << outprefix << endl;
    NEWIMAGE::save_volume(vol, outprefix);
  }
  if (split.volume_labels(labels)) {
    cout << "Saving volume labels to " << (outprefix + "_structures") << endl;
    NEWIMAGE::save_volume(labels, outprefix + "_structures");
  }

  for (auto struc : split.surface_structures()) {
    string suffix;
    auto   img       = split.surface_data(struc, suffix);
    auto   strucname = cifti::StructureEnum::toGuiName(struc);
    string fname     = outprefix + "." + strucname + suffix;

    if (img != NULL) {
      cout << "Saving " << strucname <<
        " surface data to " << fname << endl;
      giftiio::gifti_write_image(img, fname.c_str(), 1);
    }

    giftiio::gifti_free_image(img);
  }
}
